<?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
					
            <div id="services" style="padding-bottom: 60px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-section">
                                <center><h2>OUR SERVICES</h2></center>
						    </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/video-production.png" alt="" />
                                </div>
                                <h4>VIDEO PRODUCTION</h4>
                                </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/photo-production.png" alt="" />
                                </div>
                                <h4>PHOTO PRODUCTION</h4>
                                </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/web-development.png" alt="" />
                                </div>
                                <h4>WEB DEVELOPMENT & DESIGN</h4>
                           </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/post-production.png" alt="" />
                                </div>
                                <h4>POST PRODUCTION</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div id="vp-service">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
							<h4>VIDEO PRODUCTION</h4>
							<p>
							  We deliver powerful storytelling to your desired audience through the art of the moving image.
							  <ul>
									<li>Advertising Video</li>
									<li>Campaign Video</li>
									<li>Company Profile Video</li>
									<li>Documentation VideoCompany Profile Video
									<li>Events Video</li>
									<li>Infographics Video</li>
									<li>Product Knowledge Video</li>
									<li>Social Media Teaser Video</li>
									<li>YouTube Advertising Video</li>
									<li>Safety Induction Video</li>
									Etc.
							  </ul>
							</p>
						</div>
						<div class="col-md-6">
							<img src="<?php echo $baseUrl;?>/images/VP.png" alt="" />
						</div>
					</div>
				</div>
			</div>
			<div id="pp-service">
                <div class="container">
                    <div class="row">
						<div class="col-md-6">
							<img src="<?php echo $baseUrl;?>/images/PP.png" alt="" />
						</div>
                        <div class="col-md-6">
							<h4>PHOTO PRODUCTION</h4>
							<p>
							  We deliver powerful storytelling to your desired audience through the art of the moving image.
							  <ul>
									<li>Advertising Video</li>
									<li>Campaign Video</li>
									<li>Company Profile Video</li>
									<li>Documentation VideoCompany Profile Video
									<li>Events Video</li>
									<li>Infographics Video</li>
									<li>Product Knowledge Video</li>
									<li>Social Media Teaser Video</li>
									<li>YouTube Advertising Video</li>
									<li>Safety Induction Video</li>
									Etc.
							  </ul>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div id="wd-service">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
							<h4>WEB DEVELOPMENT & DESIGN</h4>
							<p>
							  We deliver powerful storytelling to your desired audience through the art of the moving image.
							  <ul>
									<li>Advertising Video</li>
									<li>Campaign Video</li>
									<li>Company Profile Video</li>
									<li>Documentation VideoCompany Profile Video
									<li>Events Video</li>
									<li>Infographics Video</li>
									<li>Product Knowledge Video</li>
									<li>Social Media Teaser Video</li>
									<li>YouTube Advertising Video</li>
									<li>Safety Induction Video</li>
									Etc.
							  </ul>
							</p>
						</div>
						<div class="col-md-6">
							<img src="<?php echo $baseUrl;?>/images/WD.png" alt="" />
						</div>
					</div>
				</div>
			</div>
			<div id="post-service">
                <div class="container">
                    <div class="row">
						<div class="col-md-6">
							<img src="<?php echo $baseUrl;?>/images/POST.png" alt="" />
						</div>
                        <div class="col-md-6">
							<h4>POST PRODUCTION</h4>
							<p>
							  We deliver powerful storytelling to your desired audience through the art of the moving image.
							  <ul>
									<li>Advertising Video</li>
									<li>Campaign Video</li>
									<li>Company Profile Video</li>
									<li>Documentation VideoCompany Profile Video
									<li>Events Video</li>
									<li>Infographics Video</li>
									<li>Product Knowledge Video</li>
									<li>Social Media Teaser Video</li>
									<li>YouTube Advertising Video</li>
									<li>Safety Induction Video</li>
									Etc.
							  </ul>
							</p>
						</div>
					</div>
				</div>
			</div>



